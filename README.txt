README.txt
----------
This (mini) module adds the nodetype to the "Edit" tabs title in brackets.
Example: "Edit (Page)" or "Edit (Story)" or "Edit (Panel)", ...
 instead of just "Edit"

Do you know the problem that you can't remember which node type
the current node belongs to?
Especially if you are using several not very different node types
on your site, you may (have) run into this.

The very time consuming way is to enter the Content-Section
and look up this node manually. 
This module solves the problem very lightweight so that
there is no need for heavy weight modules.

DEPENDENCIES
------------
- none -

INSTALLATION
------------
- Copy nodetypeinfo_tab directory to /sites/all/modules
- Enable module at /admin/build/modules
- That's it! No configuration needed. Your tabs are now officially pimped!

AUTHOR/MAINTAINER
-----------------
- Julian Pustkuchen (http://Julian.Pustkuchen.com)
- Development sponsored by:
    webks: websolutions kept simple (http://www.webks.de)
      and
    DROWL: Drupalbasierte Lösungen aus Ostwestfalen-Lippe (http://www.drowl.de)
